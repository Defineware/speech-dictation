Overview:-
Based on the samples supplied by Microsoft with developing Speech Recongnition systems.
This code has been converted to be used as an object, allows 1 parameter to be passed in as a string which will be outputted via speakers.

Requirements:-
This project has been built via Visual Studio Community 2015
Project needs to be run only in Release mode as SDK will throw build errors
Can be built in either x86 or x64 framework

Dependencies:-
Microsoft Speeck SDK 5.1 to be installed

Usage:
#include "Speech.h" // in file that will call the below
Speech* sp = new Speech();
sp->processText("output speech as a string");
delete sp;

Use as own risk as I do not guarantee any code provided thoughout
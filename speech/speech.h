/*

Based on the samples supplied by Microsoft with developing Speech Recongnition systems.

Created By Steven Field

*/
#ifndef SPEECH_H
#define SPEECH_H

#include "stdafx.h"
#include "sapi.h"
#include <iostream>
#include <sstream>
using namespace std;

class Speech {
public:
	Speech();
	~Speech() {}
	void processText(string text);
private:
	ISpVoice *pVoice;
	string errorMsg;
	wstringstream wss;
protected:

};


#endif

#include "speech.h"

Speech::Speech() {
	ISpVoice * pVoice = NULL;
	if (FAILED(::CoInitialize(NULL)))
		errorMsg = "Can not Initialize!";
}

void Speech::processText(string text)
{
	wss << text.c_str();
	wss.str().c_str();
	HRESULT hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&pVoice);
	if (SUCCEEDED(hr))
	{
		hr = pVoice->Speak((LPWSTR)wss.str().c_str(), 0, NULL);
		pVoice->Release();
		pVoice = NULL;
	}
	::CoUninitialize();
}
